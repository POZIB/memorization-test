const arrRoman = ['I','II','III','IV','V','VI','VII','VIII','IX'];
const arrWords = ['Один','Два','Три','Четыре','Пять','Шесть','Семь','Восемь','Девять'];
let arrOutTable = [];
let arrInputNumber = [];
var isStart = false;
var countArabicPassedTest = 0;
var countRomanPassedTest = 0;
var countWordPassedTest = 0;
var countArabicErrorTest = 0;
var countRomanErrorTest = 0;
var countWordErrorTest = 0;

//ввести статиситку общих ошибок по таблицам 

var secOut = Number($("#rangeTimerMemorization").val()); //начальное время таймера 
var secIn = Number($("#rangeTimerInput").val());


$(window).on('load', function(){windowSize(); });
$(window).on('resize', function(){ windowSize(); });

function windowSize(){
    if ($(window).width() <= '810'){
			$(".arrow").removeClass("hide");
			$('.menu').removeClass("hover");
			$('.menu').addClass("btn");
			$('.heading').removeClass("hide");
			$('.accord-content').removeClass("show");
    } else {
		$('.menu').removeClass("btn");
		$('.menu').addClass("hover");
        $(".arrow").addClass("hide");
		$('.heading').addClass("hide");
		$('.accord-content').addClass("show");
    }
}


$(".arrow").click(function() {
    $(this).toggleClass("open");
    if(!$(this).hasClass('open'))
        $('.accordion input[name=checkbox-accordion]').prop('checked',false);
	$('.menu').toggleClass("open");
});

$('#lowLevel').on('click', function () {
    if ( $(this).is(':checked') ){
    	$('.table').empty();
    	addingCell(9);
    }
})

$('#midLevel').on('click', function () {
    if ( $(this).is(':checked') ){
    	$('.table').empty();
    	addingCell(18);
    }
})

$('#highLevel').on('click', function () {
    if ( $(this).is(':checked') ){
    	$('.table').empty();
    	addingCell(27);
    }
})

function addingCell(val){
	for (var i = 0; i < val; i++){
		$('.table').append($("<div class='cell'>"));
	}
}

$('#radioInput').on('click', function () {
	$('#checkOutOrder').prop( "disabled", false );
	$('#checkWithOrder').prop( "disabled", false );
	clearTableAndInputs();
    
})

$('#radioSelect').on('click', function () {
	$('#checkOutOrder').prop( "disabled", false );
	$('#checkOutOrder').prop( "checked", true )
	$('#checkWithOrder').prop( "disabled", true ); 
	$('#checkWithOrder').prop( "checked", false )
	clearTableAndInputs();
})

//--------------------------------------
$(document).ready(function() {
    $("#rangeTimerMemorization").on("input",function(e){
    	var value = $(this).val();
		$("#outValueRange").text(value +' секунд');
		secOut = Number(value)+1; 	
	});
});

$(document).ready(function() {
    $("#rangeTimerInput").on("input",function(e){
    	var value = $(this).val();
		$("#inputValueRange").text(value +' секунд');
		secIn = Number(value)+1; 	
	});
});


$(document).ready(function() {
    $("#btnStart").click(function(){	
    	if($('input[name="level"]:checked').length === 0) {
    		notification(false,'Выберите уровень сложности!');
    		return;
		}
		isStart = true;
		offBtn();
		timerMemo();
    	if ($('#lowLevel').prop('checked')) {
        	generationValue(9,"low");
    	} else
    	if ( $('#midLevel').prop('checked')) {
        	generationValue(18,"mid");

   		} else
    	if ( $('#highLevel').prop('checked')) {
        	generationValue(27,"high");
    	} 
	});
});

let timerMemorization;
function timerMemo() {
	var secBlock = $('#secTimer');
    if (secOut == undefined) secOut = 0;
    if (secOut != 0) secOut--;
    if (secOut === 0){
    	secOut = Number($("#rangeTimerMemorization").val())+1;
    	secBlock.text('');
	    readingTableValue();
	    generationInputs();
	    $('.table div').empty();
	    	if ($('#midLevel').prop('checked') ||  $('#highLevel').prop('checked')){
	    		secIn = Number($('#rangeTimerInput').val())+1;
	    		timerIn();
	    	}
    	return;
    }
    secBlock.text(secOut);
    /*перезапускаем функцию, через секунду*/
    if (timerMemorization !== null) clearInterval(timerMemorization)
   
    timerMemorization = setTimeout("timerMemo()", 1000);
}

let timerInput;
function timerIn() {
	var secBlock = $('#secTimer');
    if (secIn == undefined) secIn = 0;
    if (secIn != 0) secIn--;
    if (secIn === 0){
    	secIn = Number($("#rangeTimerInput").val())+1;
    	secBlock.text('');
    	notification(false,"Время вышло!");
    	setTimeout(function(){
			$('#btnStop').trigger('click');
		},3000);
    	return ;
    }
    secBlock.text(secIn);
    /*перезапускаем функцию, через секунду*/
    if (timerInput !== null) clearInterval(timerInput)
   
    timerInput = setTimeout("timerIn()", 1000);
}

var isCheck = false;
$(document).ready(function() {
    $("#btnCheck").click(function(){
    	if (!isStart){
    		notification(false,"Начните тест");
    		return;
    	}
    	if ($('.inputs-group').children().length === 0) {
    		notification(false,"Дождитесь завершения отсчета таймера");  
    		return;
    	}
    	if (arrOutTable.length !== readingInputsValue()){
			var length = Number(arrOutTable.length)-Number(readingInputsValue());
    		notification(false,endingNotific(length));
    		return;
    	}
    	if (isCheck) return;
    	clearInterval(timerInput);
		if (compareTableAndInputsValue()) {
			isCheck = true;
			notification(true,'Правильно');
			setTimeout(function(){
				isCheck = false;
				// $('#btnStart').trigger('click');
				$('#btnStop').trigger('click');
			},3000);
		} 
		else{
			isCheck = true;
			notification(false,'Не правильно');
			setTimeout(function(){
				isCheck = false;
				// $('#btnStart').trigger('click');
				$('#btnStop').trigger('click');
			},3000);
		}
    }); 	
}); 

function endingNotific(length){
	var sms;
	var str;

	if (length > 0){
		if (length === 1)	str = ' число';
		if (length !== 1 && length < 5) str = ' числа';
		if (length > 4) str = ' чисел';
		sms = "Осталось выбрать/ввести "+ String(length) +str;
	}else {
		if (length === -1)	str = ' лишнее число';
		if (length !== -1 && length > -5) str = ' лишних числа';
		if (length < -4) str = ' лишних чисел';
		sms = "Упс, вы выбрали "+ String(-length) + str;
	}
	return sms;
}


$(document).ready(function() {
    $("#btnStop").click(function(){
    	isStart = false;
    	onBtn();
    	clearInterval(timerMemorization);
    	clearInterval(timerInput);
		clearTableAndInputs();
		secOut = Number($("#rangeTimerMemorization").val())+1;
		secIn = Number($("#rangeTimerInput").val())+1;
    	$('#secTimer').text('');
    }); 
});

function generationValue(quantityCell,level){
	let arrRandomValue = [];
	let arrRandomCell = [];
	clearTableAndInputs();
	if (level == "low") level = Number(4);
	if (level == "mid")	level = Number(6);
	if (level == "high") level = Number(8);

	while(arrRandomValue.length < level){
		var rand = Math.floor(Math.random() * 9) + 1;
		if (arrRandomValue.indexOf(rand) == -1)
			arrRandomValue.push(rand);	
	}

	for (let i = 0; i < quantityCell; i++){
		var rand = String(Math.floor(Math.random() * quantityCell));
		if (arrRandomCell.indexOf(rand) == -1)
			arrRandomCell.push(rand);		
	}

	if ($('#numeralsArabic').prop('checked')){			
		for (let i = 0; i < (arrRandomValue.length); i++){
			if (arrRandomValue[i] != undefined){
				$( ".table div" ).eq(Number(arrRandomCell[i])).append("<h2>" +arrRandomValue[i]+ "</h2>")
			}
		}
	}	

	if ($('#numeralsRoman').prop('checked')){		
		for (let i = 0; i < (arrRandomValue.length); i++){
			if (arrRandomValue[i] != undefined){		
				$(".table div").eq(Number(arrRandomCell[i])).append("<h2>" +arrRoman[arrRandomValue[i]-1]+ "</h2>")
			}
		}
	}

	if ($('#numeralsWord').prop('checked')){	
		for (let i = 0; i < (arrRandomValue.length); i++){
			if (arrRandomValue[i] != undefined){
				$( ".table div" ).eq(Number(arrRandomCell[i])).append("<h2>" +arrWords[arrRandomValue[i]-1]+ "</h2>")
			}
		}
	}
 }


function generationInputs(){
	console.log("table: "+arrOutTable)

	if ($('#radioSelect').prop('checked')){	
		$('.inputs-group').append("<div class='check-group'></div>");

		for (let i = 1; i < 10; i++){
			let str = "<div class='check-item'> <input type='checkbox' class='check-value' id='check"+i+
			 "' ><label for='check"+i+"'>"+i+"</label></div>";

			$('.check-group').append(str);
		}
	}
	if ($('#radioInput').prop('checked')){
		for (let i = 0; i < arrOutTable.length; i++){
			$('.inputs-group').append("<input type='number' name='inputFields'>");
		}
	}
}


function readingTableValue(){
	arrOutTable = [];

	$('.table div').children("h2").each(function(i) {
	 	arrOutTable.push($(this).text()) 
	});
}


function readingInputsValue(){
	arrInputNumber = [];
	let inc = 0;

	if ($('#radioSelect').prop('checked')){	
		$('.check-group .check-item').each(function(e) {
			if ($(this).children("input").prop('checked')){	 
				arrInputNumber.push(String(e+1));
				inc++;
			}
		});
	}

	if ($('#radioInput').prop('checked')){	
		$('.inputs-group').children("input").each(function(e) {
			if ($(this).val() === '' || $(this).val() === null) 
				$(this).css("background", "#ffb5b5");
    		else{ 		
    			arrInputNumber.push($(this).val());
    			inc++; 
    		}
		});
	}
	return inc;
}

 function compareTableAndInputsValue(){
	let countError = 0;
 	readingInputsValue();

	//сравнение с привязкой к порядку ввода
	if ($('#checkWithOrder').prop('checked')){
		if ($('#numeralsArabic').prop('checked')){
			for (let i = 0; i < arrOutTable.length; i++){
				if (arrInputNumber[i] != arrOutTable[i]){
					countArabicErrorTest++;
					countError++; 
				}
			}
			countArabicPassedTest++;
		}
		if ($('#numeralsRoman').prop('checked')){
			for (let i = 0; i < arrOutTable.length; i++) {
				if (arrInputNumber[i] != arrRoman.indexOf(arrOutTable[i])+1){
					countRomanErrorTest++;
					countError++;
				}
			}
			countRomanPassedTest++;
		}
		if ($('#numeralsWord').prop('checked')){
			for (let i = 0; i < arrOutTable.length; i++) {
				if (arrInputNumber[i] != arrWords.indexOf(arrOutTable[i])+1){
					countWordErrorTest++;
					countError++;
				}
			}
			countWordPassedTest++;
		}
 	}else

	//сравнение без привязки к порядку ввода
 	if ($('#checkOutOrder').prop('checked')){
		if ($('#numeralsArabic').prop('checked')){
			for (let i = 0; i < arrOutTable.length; i++){
				if (arrInputNumber.indexOf(arrOutTable[i]) === -1){
					countArabicErrorTest++;
					countError++;
				}
			}
			countArabicPassedTest++;
		}
		if ($('#numeralsRoman').prop('checked')){
			for (let i = 0; i < arrOutTable.length; i++) {
				if (arrInputNumber.indexOf(String(arrRoman.indexOf(arrOutTable[i])+1)) == -1){
					countRomanErrorTest++;
					countError++;
				}
			}
			countRomanPassedTest++;
		}
		if ($('#numeralsWord').prop('checked')){
			for (let i = 0; i < arrOutTable.length; i++) {
				if (arrInputNumber.indexOf(String(arrWords.indexOf(arrOutTable[i])+1)) == -1){
					countWordErrorTest++;
					countError++;
				}
			}
			countWordPassedTest++;
		}
 	}
	outStatistic(countError);
	outСonclusion();
 	if (countError > 0) return false
	 else return true
}


function notification(bool,sms){
	if (bool){
		$("#message").text(sms);
		$(".notifications").addClass("notifications-success");
	}
	else{
		$("#message").text(sms);
		$(".notifications").addClass("notifications-failure");
	}
	setTimeout(function(){
		$("#message").text('');
		$(".notifications").removeClass("notifications-success");
		$(".notifications").removeClass("notifications-failure");
	},3000);
	}


function outStatistic(error){
	let table = [];
	let input = [];

	for (let i = 0; i < arrOutTable.length; i++){
		if (arrOutTable[i] !== undefined )
			table.push('[' + arrOutTable[i] + ']'); 
	}

	for (let i = 0; i < arrInputNumber.length; i++){
		if (arrInputNumber[i] !== undefined )
			input.push('[' + arrInputNumber[i] + ']'); 
	}


	let valid = (error === 0) ? "right" : "wrong";
	let out = "<div class='outFiled "+ valid +"'><div class='valuesTableInputs'><p>"+ table +"</p><p>"+ input +"</p></div><p>Ошибок: "+ error +"</p></div>";

	if ($('#numeralsArabic').prop('checked')) $("#statisticArab").append(out);
	if ($('#numeralsRoman').prop('checked')) $("#statisticRoman").append(out)	
	if ($('#numeralsWord').prop('checked')) $("#statisticWord").append(out);
}

function outСonclusion(){
	$(".conclusion div").empty();
	let str;
	let arrArabic = endingStatistic(countArabicPassedTest,countArabicErrorTest);
	let arrRoman = endingStatistic(countRomanPassedTest,countRomanErrorTest);
	let arrWord = endingStatistic(countWordPassedTest,countWordErrorTest);

	let arabic = "Вы прошли "+countArabicPassedTest+" "+arrArabic[0]+" на запоминание арабских цифр, в "+arrArabic[1]+" совершили "+countArabicErrorTest+" "+arrArabic[2];
	let roman = ", "+countRomanPassedTest +" "+arrRoman[0]+" на запоминание римских цифр, в "+arrRoman[1]+" совершили "+ countRomanErrorTest +" "+arrRoman[2];
	let word = ", "+countWordPassedTest +" "+arrWord[0]+" на запоминание цифр в словесной форме, в "+arrWord[1]+" совершили "+countWordErrorTest +" "+arrWord[2]+".";
	

	if (countArabicPassedTest === 0 && countRomanPassedTest === 0){
		str = "Вы прошли "+countWordPassedTest+" "+arrWord[0]+" на запоминание цифр в словесной форме, в "+arrWord[1]+" совершили "+countWordErrorTest+" "+arrWord[2];
	}
	if (countArabicPassedTest === 0 && countWordPassedTest === 0){
		str = "Вы прошли "+countRomanPassedTest+" "+arrRoman[0]+" на запоминание римских цифр, в "+arrRoman[1]+" совершили "+countRomanErrorTest+" "+arrRoman[2];
	}
	if (countArabicPassedTest === 0 && countRomanPassedTest !== 0 && countWordPassedTest !== 0){
		str = "Вы прошли "+countRomanPassedTest+" "+arrRoman[0]+" на запоминание римских цифр, в "+arrRoman[1]+" совершили "+countRomanErrorTest+" "+arrRoman[2];
		str += word;
	}

	if (countArabicPassedTest !== 0){
		str = arabic;
		if (countRomanPassedTest > 0) str += roman;
		if (countWordPassedTest > 0) str +=word;
	}

	let out = "<p>"+str+"</p>";

	$(".conclusion div").append(out);
	
	// var arrErrorTest = [];
	// arrErrorTest[0] = (isNaN(Math.round(countArabicErrorTest/countArabicPassedTest))) ? 0 : Math.round(countArabicErrorTest/countArabicPassedTest);
	// arrErrorTest[1] = (isNaN(Math.round(countRomanErrorTest/countRomanPassedTest))) ? 0 : Math.round(countRomanErrorTest/countRomanPassedTest);
	// arrErrorTest[2] = (isNaN(Math.round(countWordErrorTest/countWordPassedTest))) ? 0 : Math.round(countWordErrorTest/countWordPassedTest);

	let arrErrorTest = [countArabicErrorTest,countRomanErrorTest,countWordErrorTest];
	
	let maxIndex = arrErrorTest.indexOf(Math.max.apply(null, arrErrorTest));
	let countArr = 0;
	//var minIndex = arrErrorTest.indexOf(Math.min.apply(null, arrErrorTest));
	let min = arrErrorTest[0]; // минимальное число
	let minIndex = 0;      // индекс минимального элемента

	for (let i = 0; i < arrErrorTest.length; i++) // 
	{
		arrErrorTest[i] > 0 ? countArr++ : 0;
		if (min > arrErrorTest[i] && arrErrorTest[i] >= 1)
		{
			min = arrErrorTest[i];
			minIndex = i;
		}
	}

    let nameMaxNumerals;
	let nameMinNumerals;

	 if (countArr > 1){
		if (maxIndex === minIndex){
			str = "Пока у вас одинаковый результат во пройденных всех тестах!";
			$(".conclusion div").append("<p>"+str+"</p>");
			return;
		}

		if (maxIndex === 0) nameMaxNumerals = "Арабских цифрах";
		if (maxIndex === 1) nameMaxNumerals = "Римских цифрах";
		if (maxIndex === 2) nameMaxNumerals = "цифрах в словесной форме";
		
		if (minIndex === 0) nameMinNumerals = "Арабских цифрах";
		if (minIndex === 1) nameMinNumerals = "Римских цифрах";
		if (minIndex === 2) 
			str = "Чаще всего вы совершали ошибки в "+nameMaxNumerals+"<br>"+"Меньше всего вы совершали ошибки в цифрах в словесной форме"+"<br>"+"Вывод: Вам лучше всего дается запоминание цифр в словесной форме";
		else  
			str = "Чаще всего вы совершали ошибки в "+nameMaxNumerals+"<br>"+"Меньше всего вы совершали ошибки в "+ nameMinNumerals+"<br>"+"Вывод: Вам лучше всего дается запоминание "+ nameMinNumerals.slice(0,-2);
		
	 	$(".conclusion div").append("<p>"+str+"</p>");
	}

}


function endingStatistic(test,error){
	let arr = ['','которых',''];

	if (test === 0)	arr[0] = 'тестов';
	if (test === 1){ arr[0] = 'тест'; arr[1] = 'котором'; }
	if (test > 1 && test < 6) arr[0] = 'теста';
	if (test > 5) arr[0] = 'тестов';


	if (error === 0)	arr[2] = 'ошибок';
	if (error === 1)	arr[2] = 'ошибку';
	if (error > 1 && error < 6)	arr[2] = 'ошибки';
	if (error > 5)	 arr[2] = 'ошибок';
	return arr;
}


function offBtn(){
	$("#btnStart").prop('disabled', true);
	$("#lowLevel").prop('disabled', true);
	$("#midLevel").prop('disabled', true);
	$("#highLevel").prop('disabled', true);
	$("#numeralsArabic").prop('disabled', true);
	$("#numeralsRoman").prop('disabled', true);
	$("#numeralsWord").prop('disabled', true);
	$("#radioSelect").prop('disabled', true);
	$("#radioInput").prop('disabled', true);
	$('.slider').prop('disabled', true);
}

function onBtn(){
	$("#btnStart").prop('disabled', false);
	$("#lowLevel").prop('disabled', false);
	$("#midLevel").prop('disabled', false);
	$("#highLevel").prop('disabled', false);
	$("#numeralsArabic").prop('disabled', false);
	$("#numeralsRoman").prop('disabled', false);
	$("#numeralsWord").prop('disabled', false);
	$("#radioSelect").prop('disabled', false);
	$("#radioInput").prop('disabled', false);
	$('.slider').prop('disabled', false);
} 

function clearTableAndInputs(){
	$('.table').children("div").empty();
	$('.inputs-group').empty();
}

function startSet(){
	$("#outValueRange").text(secOut +' секунд');
	$("#inputValueRange").text(secIn +' секунд');
}

document.addEventListener('DOMContentLoaded',startSet);